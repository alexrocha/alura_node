var mysql = require('mysql');
function createDBConnection(){
  var database = process.env.NODE_ENV == 'test' ? 'casadocodigo_nodejs_test' : 'casadocodigo_nodejs';
  return  mysql.createConnection({
            host : 'localhost',
            user : 'root',
            password : '',
            database : database
          });
}

module.exports = function(){
    return createDBConnection;
}