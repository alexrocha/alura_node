var express = require('../config/express')();
var request = require('supertest')(express);

describe('ProdutosController',function(){

    beforeEach(function(done){
      var conn = express.infra.connectionFactory();
      conn.query('truncate produtos;', function(ex, result){
        if(!ex){
          done();
        };
      });
    })

    it('listagem json',function(done){
        request.get('/produtos')
        .set('Accept','application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    });

    it('#cadastro de produtos com dados inválidos', function(done){
      request.post('/produtos')
      .send({titulo: '', descrição: 'novo livro'})
      .expect(400, done)
    });

    it('#cadastro de produtos com dados validos', function(done){
      request.post('/produtos')
      .send({titulo: 'novo livro de node', descrição: 'novo livro', preco: '100'})
      .expect(302, done)
    });
});